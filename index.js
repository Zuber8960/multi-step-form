const sideBar = document.querySelector(".side-bar");
const numbers = document.querySelectorAll(".number");
const choice = document.getElementById("choice");
const plansInPlanInfo = document.querySelectorAll(".plan");
const checkBoxes = document.querySelectorAll(".checkbox");
const checkPlanName = document.getElementById("checkPlanName");
const checkPlanPrice = document.getElementById("checkPlanPrice");
const checkFinalServices = document.getElementById("checkFinalServices");
const totalOfAllPlans = document.getElementById("totalOfAllPlans");

const planTypeSpan = document.getElementById("planTypeSpan");



const userName = document.getElementById("name");
const userEmail = document.getElementById("email");
const userTel = document.getElementById("tel");


let userPlan = "monthly";
let currentPlanInPlanInfo;
let planClicked = false;

const container = document.querySelector(".main-container");

const personalContainer = document.querySelector(".inner-personal-container");
const planContainer = document.querySelector(".inner-plan-container");
const addOnsContainer = document.querySelector(".inner-pick-add-ons-container");
const finishContainer = document.querySelector(".inner-finish-container");
const thanksContainer = document.querySelector(".thanks");

numbers[0].classList.add("currentPage");

const pickAddOnPlans = document.querySelectorAll(".pick-add-ons-plan");


// console.log(sideBar);

container.addEventListener("click", goToPageAndOtherOperations);

function goToPageAndOtherOperations(event) {

    // if click on next page button
    if (event.target.classList.contains("nextPage")) {
        event.preventDefault();

        // to go personal-info page to plan-info
        if (event.target.parentElement.parentElement.classList.contains("personal-info")) {

            if (personalInfoValidation(userName.value, userEmail.value, userTel.value)) {
                numbers[0].classList.remove("currentPage");
                numbers[1].classList.add("currentPage");

                personalContainer.style.display = "none";
                planContainer.style.display = "block";
            }


        } else if (event.target.parentElement.parentElement.classList.contains("plan-info")) {


            checkBoxes.forEach(checkBox => {
                checkBox.checked = false;
            });

            if (userPlan === "yearly") {
                pickAddOnPlans[0].innerText = "+$10/yr";
                pickAddOnPlans[1].innerText = "+$20/yr";
                pickAddOnPlans[2].innerText = "+$20/yr";
            } else if (userPlan === "monthly") {
                pickAddOnPlans[0].innerText = "+$1/mo";
                pickAddOnPlans[1].innerText = "+$2/mo";
                pickAddOnPlans[2].innerText = "+$2/mo";
            }

            // to go to plan-info page to add-ons-page

            if (planClicked === true) {
                numbers[1].classList.remove("currentPage");
                numbers[2].classList.add("currentPage");

                planContainer.style.display = "none";
                addOnsContainer.style.display = "block";
            }


        // to go to add-ons-page to finish page

        } else if (event.target.parentElement.parentElement.classList.contains("add-ons")) {

            numbers[2].classList.remove("currentPage");
            numbers[3].classList.add("currentPage");

            addOnsContainer.style.display = "none";
            finishContainer.style.display = "block";

            checkPlanName.innerText = currentPlanInPlanInfo[0];
            checkPlanPrice.innerText = currentPlanInPlanInfo[2];
            if (userPlan === "yearly") {
                checkPlanName.nextElementSibling.innerText = '(Yearly)';
                planTypeSpan.innerText = "year";
            } else {
                checkPlanName.nextElementSibling.innerText = '(Monthly)';
                planTypeSpan.innerText = "month";
            };

            const serviceName1 = document.querySelectorAll(".service")[0].dataset.serviceName;
            const serviceName2 = document.querySelectorAll(".service")[1].dataset.serviceName;
            const serviceName3 = document.querySelectorAll(".service")[2].dataset.serviceName;

            const servicePrice1 = document.querySelectorAll(".service")[0].dataset.servicePrice;
            const servicePrice2 = document.querySelectorAll(".service")[1].dataset.servicePrice;
            const servicePrice3 = document.querySelectorAll(".service")[2].dataset.servicePrice;

            if (serviceName1) {
                checkFinalServices.appendChild(addDivInPlan(serviceName1, servicePrice1));
            }
            if (serviceName2) {
                checkFinalServices.appendChild(addDivInPlan(serviceName2, servicePrice2));
            }
            if (serviceName3) {
                checkFinalServices.appendChild(addDivInPlan(serviceName3, servicePrice3));
            };


            // console.log(planPrice);

            let totalPlan = "mo";
            if (userPlan === "yearly") {
                totalPlan = "yr";
            } else {
                totalPlan = "mo";
            }

            totalOfAllPlans.innerText = `+$${gettingTotal(checkPlanPrice, checkFinalServices)}/${totalPlan}`
                ;


            function addDivInPlan(serviceName, servicePrice) {
                const serviceDiv = document.createElement("div");
                serviceDiv.classList.add("final-service");
                const span = document.createElement("span");
                span.classList.add("gray");
                span.innerText = serviceName;

                const paragraph = document.createElement("p");
                paragraph.classList.add("gray");
                paragraph.innerText = servicePrice;

                serviceDiv.appendChild(span);
                serviceDiv.appendChild(paragraph);
                return serviceDiv;
            };

        // to go to finish page to thanks page

        } else if (event.target.parentElement.parentElement.classList.contains("finish")) {

            finishContainer.style.display = "none";
            thanksContainer.style.display = "block";
        }
    } else if (event.target.classList.contains("goBack")) {
        event.preventDefault();
        if (event.target.parentElement.parentElement.classList.contains("plan-info")) {
            numbers[1].classList.remove("currentPage");
            numbers[0].classList.add("currentPage");

            planContainer.style.display = "none";
            personalContainer.style.display = "block";
        } else if (event.target.parentElement.parentElement.classList.contains("add-ons")) {

            numbers[2].classList.remove("currentPage");
            numbers[1].classList.add("currentPage");

            addOnsContainer.style.display = "none";
            planContainer.style.display = "block";


            document.querySelectorAll("[data-service-name]").forEach(element => {
                delete element.dataset.serviceName;
                delete element.dataset.servicePrice;
            });

        } else if (event.target.parentElement.parentElement.classList.contains("finish")) {

            numbers[3].classList.remove("currentPage");
            numbers[2].classList.add("currentPage");

            finishContainer.style.display = "none";
            addOnsContainer.style.display = "block";

            checkFinalServices.innerHTML = null;
        }
    } else if (event.target.className === "plan") {
        // console.log("clicked");
        planClicked = true;

        plansInPlanInfo[0].style.border = "1px solid lightgray";
        plansInPlanInfo[1].style.border = "1px solid lightgray";
        plansInPlanInfo[2].style.border = "1px solid lightgray";

        event.target.style.border = "2px solid #174A8B";

        currentPlanInPlanInfo = event.target.innerText.split("\n");
        // console.log(currentPlanInPlanInfo);

    } else if (event.target.classList.contains("checkbox")) {
        // console.log(event.target.nextElementSibling);
        if (event.target.checked) {
            event.target.parentElement.dataset.serviceName = event.target.nextElementSibling.firstElementChild.innerText;
            event.target.parentElement.dataset.servicePrice = event.target.nextElementSibling.nextElementSibling.innerText;
        } else {
            delete event.target.parentElement.dataset.serviceName;
            delete event.target.parentElement.dataset.servicePrice;
        }

    } else if (event.target.dataset.change === "change") {

        numbers[1].classList.add("currentPage");
        numbers[3].classList.remove("currentPage");

        document.querySelectorAll("[data-service-name]").forEach(element => {
            delete element.dataset.serviceName;
            delete element.dataset.servicePrice;
        });

        checkFinalServices.innerHTML = null;

        finishContainer.style.display = "none";
        planContainer.style.display = "block";
    }
}




function gettingTotal(checkPlanPrice, checkFinalServices) {
    let resArr = [];

    resArr.push(regax(checkPlanPrice.innerText));


    if (checkFinalServices.children.length >= 1) {
        resArr.push(regax(checkFinalServices.children[0].lastElementChild.innerText));
    }
    if (checkFinalServices.children.length >= 2) {
        resArr.push(regax(checkFinalServices.children[1].lastElementChild.innerText));
    }
    if (checkFinalServices.children.length >= 3) {
        resArr.push(regax(checkFinalServices.children[2].lastElementChild.innerText));
    };

    // console.log(resArr);
    const sum = resArr.reduce((total, curr) => {
        total += (+curr);
        return total;
    }, 0);
    return sum;
}

function regax(num) {
    return num.match(/[0-9]/g).join("");
}



const plantype = document.querySelectorAll(".planType");
const plans = document.querySelectorAll(".plan-data");
// console.log(plans[1]);
choice.value = 0;


function changePlan() {

    planClicked = false;

    plansInPlanInfo[0].style.border = "1px solid lightgray";
    plansInPlanInfo[1].style.border = "1px solid lightgray";
    plansInPlanInfo[2].style.border = "1px solid lightgray";

    if (choice.value === "1") {
        plantype[0].innerText = "$90/yr";
        addDivForYearlyPlan(plans[0]);
        plantype[1].innerText = "$120/yr";
        addDivForYearlyPlan(plans[1]);
        plantype[2].innerText = "$150/yr";
        addDivForYearlyPlan(plans[2]);


        userPlan = "yearly";
    } else if (choice.value === "0") {
        const planIds = document.querySelectorAll(".planId");

        userPlan = "monthly";
        if (planIds.length) {
            planIds[0].remove();
            planIds[1].remove();
            planIds[2].remove();
        }

        plantype[0].innerText = "$9/mo";
        plantype[1].innerText = "$12/mo";
        plantype[2].innerText = "$15/mo";

    }
}


function addDivForYearlyPlan(div) {
    const p = document.createElement("p");
    p.classList.add("size12px", "bold", "planId");
    p.innerText = "2 months free";
    div.appendChild(p);
}




function personalInfoValidation(name, email, tel) {
    if (checkName(name)) {
        document.querySelector(".name").innerText = "Enter valid name";
    } else {
        document.querySelector(".name").innerText = null;
    }
    if (emailCheck(email)) {
        document.querySelector(".email").innerText = "Enter valid email";
    } else {
        document.querySelector(".email").innerText = null;
    }
    if (checkPhone(tel)) {
        document.querySelector(".tel").innerText = "Enter valid Phone number";
    } else {
        document.querySelector(".tel").innerText = null;
    }

    if (!document.querySelector(".name").innerText && !document.querySelector(".email").innerText && !document.querySelector(".tel").innerText) {
        return true
    } else {
        return false;
    }
}




function checkName(name) {
    if (name.match(/[^a-zA-Z]/) || name.length === 0) {
        return true;
    } else {
        return false;
    }
}



function emailCheck(email) {
    if (email.includes("@") === false) {
        return true;
    } else {
        return false;
    }
}


function checkPhone(phone) {
    console.log(phone, phone.length);
    const valid = /^(0|91)?[6-9][0-9]{9}$/;
    if (!valid.test(phone)) {
        return true;
    } else {
        return false;
    }
}
